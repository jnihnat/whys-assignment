"""
app URL Configuration
Used routers for basic API view of list and detail
"""
from django.contrib import admin
from django.urls import path, include
from whys import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r"attributes", views.AttributeViewSet, basename="attributes")
router.register(
    r"attribute-names", views.AttributeNameViewSet, basename="attribute-names"
)
router.register(
    r"attribute-values", views.AttributeValueViewSet, basename="attribute-values"
)
router.register(r"products", views.ProductViewSet, basename="products")
router.register(
    r"product-atributes", views.ProductAttributeViewSet, basename="product-attributes"
)
router.register(r"product-images", views.ProductImageViewSet, basename="product-images")
router.register(r"images", views.ImageViewSet, basename="images")
router.register(r"catalogs", views.CatalogViewSet, basename="catalogs")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include(router.urls)),
    path("import/", views.ImportAll.as_view(), name="import"),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
