from django.db import models


class Image(models.Model):
    """
    Model for saving url to product images
    """

    image = models.URLField("Image")

    def __str__(self):
        return self.image


class Catalog(models.Model):
    """
    Model for information about catalog
    """

    name = models.CharField("Catalog name", max_length=50, blank=True, null=True)
    image_id = models.ForeignKey(Image, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name


class AttributeName(models.Model):
    """
    Model for names of the attribute.
    """

    name = models.CharField("Attribute name", max_length=100)
    show = models.BooleanField("Show", default=True, blank=True, null=True)
    code = models.CharField("Code", max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name


class AttributeValue(models.Model):
    """
    Model for values of the attribute
    """

    value = models.CharField("Attribute value", max_length=100)

    def __str__(self):
        return self.value


class Attribute(models.Model):
    """
    Model with attributes, foreign keys to attribute name and value
    """

    attribute_name_id = models.ForeignKey(AttributeName, on_delete=models.CASCADE)
    attribute_value_id = models.ForeignKey(AttributeValue, on_delete=models.CASCADE)
    catalog = models.ForeignKey(
        Catalog,
        related_name="attributes_ids",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.attribute_name_id.name + ": " + self.attribute_value_id.value


class Product(models.Model):
    """
    Model for basic info about product
    """

    name = models.CharField("Product name", max_length=100)
    description = models.CharField("Product Description", max_length=500)
    price = models.FloatField("Price")
    currency = models.CharField("Currency", max_length=10)
    published_on = models.DateTimeField("Published on", blank=True, null=True)
    is_published = models.BooleanField("Is published", default=False)
    catalog = models.ForeignKey(
        Catalog,
        related_name="products_ids",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name


class ProductAttribute(models.Model):
    """
    Model for relation between product and it's attributes
    """

    attribute = models.ForeignKey(Attribute, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return "Product: " + self.product.name + ", attribute name: " + self.attribute.attribute_name_id.name


class ProductImage(models.Model):
    """
    Model for relation between product and image
    """

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    image_id = models.ForeignKey(Image, on_delete=models.CASCADE)
    name = models.CharField("Name of the image", max_length=50)

    def __str__(self):
        return self.name
