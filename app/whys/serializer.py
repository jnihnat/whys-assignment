from rest_framework import serializers
from rest_framework.validators import UniqueValidator, UniqueTogetherValidator
from .models import (
    Attribute,
    AttributeName,
    AttributeValue,
    Product,
    ProductAttribute,
    ProductImage,
    Image,
    Catalog,
)
from django.core.exceptions import ObjectDoesNotExist


class AttributeNameSerializer(serializers.ModelSerializer):
    """
    Serializer for Attribute model
    """

    id = serializers.IntegerField(
        validators=[UniqueValidator(queryset=AttributeName.objects.all())]
    )
    nazev = serializers.CharField(source="name")
    kod = serializers.CharField(source="code", required=False)
    zobrazit = serializers.BooleanField(source="show", required=False)

    class Meta:
        model = AttributeName
        fields = (
            "id",
            "nazev",
            "kod",
            "zobrazit",
        )


class AttributeSerializer(serializers.ModelSerializer):
    """
    Serializer for Attribute model
    """

    id = serializers.IntegerField(
        validators=[UniqueValidator(queryset=Attribute.objects.all())]
    )
    nazev_atributu_id = serializers.PrimaryKeyRelatedField(
        source="attribute_name_id", queryset=AttributeName.objects.all()
    )
    hodnota_atributu_id = serializers.PrimaryKeyRelatedField(
        source="attribute_value_id", queryset=AttributeValue.objects.all()
    )

    class Meta:
        model = Attribute
        fields = (
            "id",
            "nazev_atributu_id",
            "hodnota_atributu_id",
        )
        validators = [
            UniqueTogetherValidator(
                queryset=Attribute.objects.all(),
                fields=["nazev_atributu_id", "hodnota_atributu_id"],
                message="Atributes nazev_atributu_id and hodnota_atributu_id should be unique in together",
            )
        ]


class AttributeValueSerializer(serializers.ModelSerializer):
    """
    Serializer for Attribute model
    """

    id = serializers.IntegerField(
        validators=[UniqueValidator(queryset=AttributeValue.objects.all())]
    )
    hodnota = serializers.CharField(source="value")

    class Meta:
        model = AttributeValue
        fields = (
            "id",
            "hodnota",
        )


class ImportAllSerializer(serializers.Serializer):
    """
    Serializer for importing exporting all models at one
    """

    Attribute = AttributeSerializer()
    AttributeName = AttributeNameSerializer()
    AttributeValue = AttributeValueSerializer()

    class Meta:
        fields = (
            "Attribute",
            "AttributeName",
            "AttributeValue",
        )


class ImageSerializer(serializers.ModelSerializer):
    """
    Serializer for Image model
    """

    id = serializers.IntegerField(
        validators=[UniqueValidator(queryset=Image.objects.all())]
    )
    obrazek = serializers.URLField(source="image")

    class Meta:
        model = Image
        fields = ("id", "obrazek")


class ProductImageSerializer(serializers.ModelSerializer):
    """
    Serializer for ProductImage model
    """

    id = serializers.IntegerField(
        validators=[UniqueValidator(queryset=ProductImage.objects.all())]
    )
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    obrazek_id = serializers.PrimaryKeyRelatedField(
        source="image_id", queryset=Image.objects.all()
    )
    nazev = serializers.CharField(source="name")

    class Meta:
        model = ProductImage
        fields = ("id", "product", "obrazek_id", "nazev")


class ProductSerializer(serializers.ModelSerializer):
    """
    Serializer for Product model
    """

    id = serializers.IntegerField()
    nazev = serializers.CharField(source="name")
    description = serializers.CharField()
    cena = serializers.FloatField(source="price")
    mena = serializers.CharField(source="currency")
    is_published = serializers.BooleanField(required=False)
    published_on = serializers.DateTimeField(
        required=False,
        allow_null=True,
    )

    class Meta:
        model = Product
        fields = (
            "id",
            "nazev",
            "description",
            "mena",
            "is_published",
            "published_on",
            "cena",
        )


class ProductAttributeSerializer(serializers.ModelSerializer):
    """
    Serializer for Product Attribute model
    """

    id = serializers.IntegerField()
    attribute = serializers.PrimaryKeyRelatedField(queryset=Attribute.objects.all())
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())

    class Meta:
        model = ProductAttribute
        fields = (
            "id",
            "attribute",
            "product",
        )


class CatalogSerializer(serializers.ModelSerializer):
    """
    Serializer for Catalog model
    """

    id = serializers.IntegerField(
        validators=[UniqueValidator(queryset=Catalog.objects.all())]
    )
    products_ids = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Product.objects.all(), required=False
    )
    attributes_ids = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Attribute.objects.all(), required=False
    )
    obrazek_id = serializers.PrimaryKeyRelatedField(
        source="image_id", queryset=Image.objects.all(), required=False
    )
    nazev = serializers.CharField(source="name", required=True)

    class Meta:
        model = Attribute
        fields = ("id", "products_ids", "attributes_ids", "obrazek_id", "nazev")

    def create(self, validated_data):
        """
        Method for created because of nested objects.
        """
        attributes_data = 0
        products_data = 0
        try:
            attributes_data = validated_data.pop("attributes_ids")
        except:
            pass
        try:
            products_data = validated_data.pop("products_ids")
        except:
            pass

        catalog = Catalog.objects.create(**validated_data)
        if attributes_data:
            for attribute_data in attributes_data:
                if Attribute.objects.get(pk=attribute_data.id):
                    obj = Attribute.objects.get(pk=attribute_data.id)
                    obj.catalog = catalog
                    obj.save(update_fields=["catalog"])
                else:
                    Attribute.objects.create(id=attribute_data.id, catalog=catalog)

        if products_data:
            for product_data in products_data:
                if Product.objects.get(pk=product_data.id):
                    obj = Product.objects.get(pk=product_data.id)
                    obj.catalog = catalog
                    obj.save(update_fields=["catalog"])
        return catalog
