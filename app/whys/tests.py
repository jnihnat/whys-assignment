from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
import json
from rest_framework import status
from rest_framework.test import (
    APITestCase,
    APIClient,
    RequestsClient,
    force_authenticate,
    APIRequestFactory,
)
from .models import (
    Attribute,
    AttributeName,
    AttributeValue,
    Product,
    ProductAttribute,
    ProductImage,
    Image,
    Catalog,
)


class AttributeViewTest(APITestCase):
    """
    Test for API GET /attributes
    """

    def setUp(self):
        attr_name = AttributeName.objects.create(name="farba")
        attr_value1 = AttributeValue.objects.create(value="modra")
        attr_value2 = AttributeValue.objects.create(value="zelena")
        attribute1 = Attribute.objects.create(
            attribute_name_id=attr_name, attribute_value_id=attr_value1
        )
        attribute2 = Attribute.objects.create(
            attribute_name_id=attr_name, attribute_value_id=attr_value2
        )

    def test_get_list_ok(self):
        url = reverse("attributes-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_get_detail_ok(self):
        url = reverse("attributes-detail", kwargs={"pk": 1})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(
            str(response.data),
            "{'id': 1, 'nazev_atributu_id': 1, 'hodnota_atributu_id': 1}",
        )


class AttributeNameViewTest(APITestCase):
    """
    Test for API GET /attribute-names
    """

    def setUp(self):
        attr_name1 = AttributeName.objects.create(name="farba", show=True)
        attr_name2 = AttributeName.objects.create(name="delka")

    def test_get_list_ok(self):
        url = reverse("attribute-names-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_get_detail_attributes_ok(self):
        url = reverse("attribute-names-detail", kwargs={"pk": 1})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)
        self.assertEqual(
            str(response.data),
            "{'id': 1, 'nazev': 'farba', 'kod': None, 'zobrazit': True}",
        )


class AttributeValueViewTest(APITestCase):
    """
    Test for API GET /attribute-values
    """

    def setUp(self):
        attr_value1 = AttributeValue.objects.create(value="zelena")
        attr_value2 = AttributeValue.objects.create(value="modra")

    def test_get_list_ok(self):
        url = reverse("attribute-values-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_get_detail_attributes_ok(self):
        url = reverse("attribute-values-detail", kwargs={"pk": 1})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(str(response.data), "{'id': 1, 'hodnota': 'zelena'}")


class ProductViewTest(APITestCase):
    """
    Test for API GET /products
    """

    def setUp(self):
        product1 = Product.objects.create(
            name="Telka", description="Taka obycajna telka", price=555, currency="CZK"
        )

    def test_get_list_ok(self):
        url = reverse("products-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_detail_attributes_ok(self):
        url = reverse("products-detail", kwargs={"pk": 1})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 7)
        self.assertEqual(
            str(response.data),
            "{'id': 1, 'nazev': 'Telka', 'description': 'Taka obycajna telka', 'mena': 'CZK', 'is_published': False, 'published_on': None, 'cena': 555.0}",
        )


class ProductAttributeViewTest(APITestCase):
    """
    Test for API GET /product-attributes
    """

    def setUp(self):
        product1 = Product.objects.create(
            name="Telka", description="Taka obycajna telka", price=555, currency="CZK"
        )
        attr_name = AttributeName.objects.create(name="farba")
        attr_value1 = AttributeValue.objects.create(value="modra")
        attribute1 = Attribute.objects.create(
            attribute_name_id=attr_name, attribute_value_id=attr_value1
        )
        prod_attr = ProductAttribute.objects.create(
            attribute=attribute1, product=product1
        )

    def test_get_list_ok(self):
        url = reverse("product-attributes-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_detail_attributes_ok(self):
        url = reverse("product-attributes-detail", kwargs={"pk": 1})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(str(response.data), "{'id': 1, 'attribute': 1, 'product': 1}")


class ProductImageViewTest(APITestCase):
    """
    Test for API GET /product-images
    """

    def setUp(self):
        product1 = Product.objects.create(
            name="Telka", description="Taka obycajna telka", price=555, currency="CZK"
        )
        image = Image.objects.create(image="www.google.com")
        prod_image = ProductImage.objects.create(
            product=product1, image_id=image, name="Obrazok"
        )

    def test_get_list_ok(self):
        url = reverse("product-images-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_detail_attributes_ok(self):
        url = reverse("product-images-detail", kwargs={"pk": 1})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)
        self.assertEqual(
            str(response.data),
            "{'id': 1, 'product': 1, 'obrazek_id': 1, 'nazev': 'Obrazok'}",
        )


class ImageViewTest(APITestCase):
    """
    Test for API GET /images
    """

    def setUp(self):
        image = Image.objects.create(image="www.google.com")

    def test_get_list_ok(self):
        url = reverse("images-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_detail_attributes_ok(self):
        url = reverse("images-detail", kwargs={"pk": 1})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(str(response.data), "{'id': 1, 'obrazek': 'www.google.com'}")


class CatalogViewTest(APITestCase):
    """
    Test for API GET /catalogs
    """

    def setUp(self):
        image = Image.objects.create(image="www.google.com")
        catalog = Catalog.objects.create(name="Vypredaj", image_id=image)

    def test_get_list_ok(self):
        url = reverse("catalogs-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_detail_attributes_ok(self):
        url = reverse("catalogs-detail", kwargs={"pk": 1})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)
        self.assertEqual(
            str(response.data),
            "{'id': 1, 'products_ids': [], 'attributes_ids': [], 'obrazek_id': 1, 'nazev': 'Vypredaj'}",
        )


class ImportViewTest(APITestCase):
    """
    Test for API POST /import
    """

    def setUp(self):
        self.user1 = User.objects.create(username="admin", password="admin")

    def test_post_import_authenticated_1object_ok(self):
        imp_cat = {"AttributeName": {"id": 3, "nazev": "Daniel Wellington DW00100164"}}
        self.client = APIClient()
        self.client.force_authenticate(user=self.user1)
        url = reverse("import")
        response = self.client.post(url, imp_cat, format="json")
        print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AttributeName.objects.count(), 1)

    def test_post_import_authenticated_2object_ok(self):
        imp_cat = [
            {"AttributeName": {"id": 1, "nazev": "Barva"}},
            {"AttributeValue": {"id": 1, "hodnota": "modrá"}},
        ]
        self.client = APIClient()
        self.client.force_authenticate(user=self.user1)
        url = reverse("import")
        response = self.client.post(url, imp_cat, format="json")
        print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AttributeName.objects.count(), 1)
        self.assertEqual(AttributeValue.objects.count(), 1)

    def test_post_import_NOTauthenticated_2object_ok(self):
        imp_cat = [
            {"AttributeName": {"id": 1, "nazev": "Barva"}},
            {"AttributeValue": {"id": 1, "hodnota": "modrá"}},
        ]
        self.client = APIClient()
        url = reverse("import")
        response = self.client.post(url, imp_cat, format="json")
        print(response)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(AttributeName.objects.count(), 0)
        self.assertEqual(AttributeValue.objects.count(), 0)

    def test_post_import_authenticated_badobject_ok(self):
        imp_cat = {"Atter": {"id": 1, "nazev": "Barva"}}
        self.client = APIClient()
        self.client.force_authenticate(user=self.user1)
        url = reverse("import")
        response = self.client.post(url, imp_cat, format="json")
        print(response)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data, {"Error 1": "Attribute in request not recognized"}
        )
        self.assertEqual(AttributeName.objects.count(), 0)
        self.assertEqual(AttributeValue.objects.count(), 0)
