from .models import (
    Attribute,
    AttributeName,
    AttributeValue,
    Product,
    ProductAttribute,
    ProductImage,
    Image,
    Catalog,
)
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from .serializer import (
    AttributeSerializer,
    AttributeNameSerializer,
    AttributeValueSerializer,
    ImportAllSerializer,
    ImageSerializer,
    ProductSerializer,
    ProductImageSerializer,
    ProductAttributeSerializer,
    CatalogSerializer,
)
from django.shortcuts import get_object_or_404


class AttributeViewSet(viewsets.ViewSet):
    """
    API endpoint for list and detail model Attribute.
    Check of pk is done in detail view.
    """

    http_method_names = [
        "get",
    ]

    def list(self, request):
        queryset = Attribute.objects.all().order_by("id")
        serializer = AttributeSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Attribute.objects.all().order_by("id")
        try:
            data = get_object_or_404(queryset, pk=pk)
        except ValueError as ex:
            return Response(data="Wrong format of data entered", status=400)
        serializer = AttributeSerializer(data)
        return Response(serializer.data)

    # queryset = Attribute.objects.all().order_by("id")
    # serializer_class = AttributeSerializer


class AttributeNameViewSet(viewsets.ViewSet):
    """
    API endpoint for list and detail model AttributeName.
    Check of pk is done in detail view.
    """

    http_method_names = [
        "get",
    ]

    def list(self, request):
        queryset = AttributeName.objects.all().order_by("id")
        serializer = AttributeNameSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = AttributeName.objects.all().order_by("id")
        try:
            data = get_object_or_404(queryset, pk=pk)
        except ValueError as ex:
            return Response(data="Wrong format of data entered", status=400)
        serializer = AttributeNameSerializer(data)
        return Response(serializer.data)


class AttributeValueViewSet(viewsets.ViewSet):
    """
    API endpoint for list and detail model AttributeValue.
    Check of pk is done in detail view.
    """

    http_method_names = [
        "get",
    ]

    def list(self, request):
        queryset = AttributeValue.objects.all().order_by("id")
        serializer = AttributeValueSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = AttributeValue.objects.all().order_by("id")
        try:
            data = get_object_or_404(queryset, pk=pk)
        except ValueError as ex:
            return Response(data="Wrong format of data entered", status=400)
        serializer = AttributeValueSerializer(data)
        return Response(serializer.data)


class ProductViewSet(viewsets.ViewSet):
    """
    API endpoint for list and detail model Product.
    Check of pk is done in detail view.
    """

    http_method_names = [
        "get",
    ]

    def list(self, request):
        queryset = Product.objects.all().order_by("id")
        serializer = ProductSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Product.objects.all().order_by("id")
        try:
            data = get_object_or_404(queryset, pk=pk)
        except ValueError as ex:
            return Response(data="Wrong format of data entered", status=400)
        serializer = ProductSerializer(data)
        return Response(serializer.data)


class ProductAttributeViewSet(viewsets.ViewSet):
    """
    API endpoint for list and detail model Product attribute.
    Check of pk is done in detail view.
    """

    http_method_names = [
        "get",
    ]

    def list(self, request):
        queryset = ProductAttribute.objects.all().order_by("id")
        serializer = ProductAttributeSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = ProductAttribute.objects.all().order_by("id")
        try:
            data = get_object_or_404(queryset, pk=pk)
        except ValueError as ex:
            return Response(data="Wrong format of data entered", status=400)
        serializer = ProductAttributeSerializer(data)
        return Response(serializer.data)


class ProductImageViewSet(viewsets.ViewSet):
    """
    API endpoint for list and detail model Product image.
    Check of pk is done in detail view.
    """

    http_method_names = [
        "get",
    ]

    def list(self, request):
        queryset = ProductImage.objects.all().order_by("id")
        serializer = ProductImageSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = ProductImage.objects.all().order_by("id")
        try:
            data = get_object_or_404(queryset, pk=pk)
        except ValueError as ex:
            return Response(data="Wrong format of data entered", status=400)
        serializer = ProductImageSerializer(data)
        return Response(serializer.data)


class ImageViewSet(viewsets.ViewSet):
    """
    API endpoint for list and detail model image.
    Check of pk is done in detail view.
    """

    http_method_names = [
        "get",
    ]

    def list(self, request):
        queryset = Image.objects.all().order_by("id")
        serializer = ImageSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Image.objects.all().order_by("id")
        try:
            data = get_object_or_404(queryset, pk=pk)
        except ValueError as ex:
            return Response(data="Wrong format of data entered", status=400)
        serializer = ImageSerializer(data)
        return Response(serializer.data)


class CatalogViewSet(viewsets.ViewSet):
    """
    API endpoint for list and detail model catalog.
    Check of pk is done in detail view.
    """

    http_method_names = [
        "get",
    ]

    def list(self, request):
        queryset = Catalog.objects.all().order_by("id")
        serializer = CatalogSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Catalog.objects.all().order_by("id")
        try:
            data = get_object_or_404(queryset, pk=pk)
        except ValueError as ex:
            return Response(data="Wrong format of data entered", status=400)
        serializer = CatalogSerializer(data)
        return Response(serializer.data)


class ImportAll(APIView):
    http_method_names = ["post"]
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):
        """
        Post method to create or update data from json.
        Method goes by item through list, and based on dict key it uses serializer for given key.
        We use 3 separate itterations through fields, because some models use PK from other model.
        If non attribute from list is in model error 400 is returned.
        if only one item in request, no iteration flow is used.
        """
        datas = request.data
        self.error_number = 1
        self.one_import_ok = 0
        self.message = dict()
        if not isinstance(request.data, list):
            """
            no iterration flow
            """
            if "Attribute" in datas:
                data = dict(datas["Attribute"])
                self.key = "Attribute"
                try:
                    Attribute.objects.get(pk=data["id"])
                    self.serializer = AttributeSerializer(
                        Attribute.objects.get(pk=data["id"]),
                        data=data,
                        partial=True,
                    )
                except:
                    self.serializer = AttributeSerializer(data=data)
            elif "AttributeName" in datas:
                data = dict(datas["AttributeName"])
                self.key = "AttributeName"
                try:
                    AttributeName.objects.get(pk=data["id"])
                    self.serializer = AttributeNameSerializer(
                        AttributeName.objects.get(pk=data["id"]),
                        data=data,
                        partial=True,
                    )
                except:
                    self.serializer = AttributeNameSerializer(data=data)
            elif "AttributeValue" in datas:
                data = dict(datas["AttributeValue"])
                self.key = "AttributeValue"
                try:
                    AttributeValue.objects.get(pk=data["id"])
                    self.serializer = AttributeValueSerializer(
                        AttributeValue.objects.get(pk=data["id"]),
                        data=data,
                        partial=True,
                    )
                except:
                    self.serializer = AttributeValueSerializer(data=data)
            elif "Image" in datas:
                data = dict(datas["Image"])
                self.key = "Image"
                try:
                    Image.objects.get(pk=data["id"])
                    self.serializer = ImageSerializer(
                        Image.objects.get(pk=data["id"]), data=data, partial=True
                    )
                except:
                    self.serializer = ImageSerializer(data=data)
            elif "Product" in datas:
                data = dict(datas["Product"])
                self.key = "Product"
                try:
                    Product.objects.get(pk=data["id"])
                    self.serializer = ProductSerializer(
                        Product.objects.get(pk=data["id"]), data=data, partial=True
                    )
                except:
                    self.serializer = ProductSerializer(data=data)
            elif "ProductImage" in datas:
                data = dict(datas["ProductImage"])
                self.key = "ProductImage"
                try:
                    ProductImage.objects.get(pk=data["id"])
                    self.serializer = ProductImageSerializer(
                        ProductImage.objects.get(pk=data["id"]),
                        data=data,
                        partial=True,
                    )
                except:
                    self.serializer = self.serializer = ProductImageSerializer(
                        data=data
                    )
            elif "Catalog" in datas:
                data = dict(datas["Catalog"])
                self.key = "Catalog"
                try:
                    Catalog.objects.get(pk=data["id"])
                    self.serializer = CatalogSerializer(
                        Catalog.objects.get(pk=data["id"]), data=data, partial=True
                    )
                except:
                    self.serializer = CatalogSerializer(data=data)
            elif "ProductAttributes" in datas:
                data = dict(datas["ProductAttributes"])
                self.key = "ProductAttributes"
                try:
                    ProductAttribute.objects.get(pk=data["id"])
                    self.serializer = ProductAttributeSerializer(
                        ProductAttribute.objects.get(pk=data["id"]),
                        data=data,
                        partial=True,
                    )
                except:
                    self.serializer = ProductAttributeSerializer(data=data)
            else:
                self.message[
                    "Error %i" % self.error_number
                ] = "Attribute in request not recognized"
                return Response(data=self.message, status=400)
            ImportAll.try_serializer(self, data)
            return Response(data=self.message, status=201)
        """
        3 Itteration flows for list in request
        """
        for dic in datas:
            for self.key in dic:
                if self.key == "AttributeName":
                    data = dict(dic[self.key])
                    try:
                        AttributeName.objects.get(pk=data["id"])
                        self.serializer = AttributeNameSerializer(
                            AttributeName.objects.get(pk=data["id"]),
                            data=data,
                            partial=True,
                        )
                    except:
                        self.serializer = AttributeNameSerializer(data=data)
                    ImportAll.try_serializer(self, data)
                elif self.key == "AttributeValue":
                    data = dict(dic[self.key])
                    try:
                        AttributeValue.objects.get(pk=data["id"])
                        self.serializer = AttributeValueSerializer(
                            AttributeValue.objects.get(pk=data["id"]),
                            data=data,
                            partial=True,
                        )
                    except:
                        self.serializer = AttributeValueSerializer(data=data)
                    ImportAll.try_serializer(self, data)
                elif self.key == "Image":
                    data = dict(dic[self.key])
                    try:
                        Image.objects.get(pk=data["id"])
                        self.serializer = ImageSerializer(
                            Image.objects.get(pk=data["id"]), data=data, partial=True
                        )
                    except:
                        self.serializer = ImageSerializer(data=data)
                    ImportAll.try_serializer(self, data)
                elif self.key == "Product":
                    data = dict(dic[self.key])
                    try:
                        Product.objects.get(pk=data["id"])
                        self.serializer = ProductSerializer(
                            Product.objects.get(pk=data["id"]), data=data, partial=True
                        )
                    except:
                        self.serializer = ProductSerializer(data=data)
                    ImportAll.try_serializer(self, data)
                elif self.key not in (
                    "Attribute",
                    "ProductImage",
                    "ProductAttributes",
                    "Catalog",
                ):
                    data = dict(dic[self.key])
                    self.message[
                        "Error %i" % self.error_number
                    ] = "Attribute '%s' in request not recognized" % (self.key)
                    self.error_number += 1

        for dic in datas:
            for self.key in dic:
                if self.key == "Attribute":
                    data = dict(dic[self.key])
                    try:
                        Attribute.objects.get(pk=data["id"])
                        self.serializer = AttributeSerializer(
                            Attribute.objects.get(pk=data["id"]),
                            data=data,
                            partial=True,
                        )
                    except:
                        self.serializer = AttributeSerializer(data=data)
                    ImportAll.try_serializer(self, data)
                if self.key == "ProductImage":
                    data = dict(dic[self.key])
                    try:
                        ProductImage.objects.get(pk=data["id"])
                        self.serializer = ProductImageSerializer(
                            ProductImage.objects.get(pk=data["id"]),
                            data=data,
                            partial=True,
                        )
                    except:
                        self.serializer = self.serializer = ProductImageSerializer(
                            data=data
                        )
                    ImportAll.try_serializer(self, data)
        for dic in datas:
            for self.key in dic:
                if self.key == "ProductAttributes":
                    data = dict(dic[self.key])
                    try:
                        ProductAttribute.objects.get(pk=data["id"])
                        self.serializer = ProductAttributeSerializer(
                            ProductAttribute.objects.get(pk=data["id"]),
                            data=data,
                            partial=True,
                        )
                    except:
                        self.serializer = ProductAttributeSerializer(data=data)
                    ImportAll.try_serializer(self, data)
                if self.key == "Catalog":
                    data = dict(dic[self.key])
                    try:
                        Catalog.objects.get(pk=data["id"])
                        self.serializer = CatalogSerializer(
                            Catalog.objects.get(pk=data["id"]), data=data, partial=True
                        )
                    except:
                        self.serializer = CatalogSerializer(data=data)
                    ImportAll.try_serializer(self, data)
        if not self.one_import_ok == 0:
            return Response(data=self.message, status=201)
        else:
            return Response(data=self.message, status=400)

    def try_serializer(self, data):
        """
        Method to validate input data, saves an error if occured or saves data.
        If error occurs save is skiped and procedure continues with next item in list.
        """
        try:
            self.serializer.is_valid(raise_exception=True)
        except ValidationError as ex:
            self.message[
                "Error %i" % self.error_number
            ] = "During import of attribute '%s' with id '%s' error occured: %s" % (
                self.key,
                data["id"],
                ex.detail,
            )
            self.error_number += 1
        else:
            self.serializer.save()
            self.one_import_ok += 1
