How to run the app
==================
To run app install first requirements from workspace folder:
``pip install -r requirements.txt``

Then create admin user by running command from workspace folder:
``python app/manage.py createsuperuser``

Then run application from workspace folder:
``python app/manage.py runserver``

