.. Whys Assignment documentation master file, created by
   sphinx-quickstart on Fri Mar 12 10:20:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Whys Assignment's documentation!
===========================================


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   basics/how_to_run
   basics/info
   apidoc/whys



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
